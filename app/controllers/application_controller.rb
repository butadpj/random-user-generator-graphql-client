class ApplicationController < ActionController::Base
  class QueryError < StandardError; end

  helper_method :current_user, :logged_in?

  CurrentUser = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
    query {
        currentUser {
            id
            username
            firstName
            lastName
        }
    }
  GRAPHQL

  def current_user
    @current_user = nil
    query = (query CurrentUser)
    @current_user = query.current_user if query
  end

  def logged_in?
    !!current_user
  end

  private

  # Public: Define request scoped helper method for making GraphQL queries.
  #
  # Examples
  #
  #   data = query(ViewerQuery)
  #   data.viewer.login #=> "josh"
  #
  # definition - A query or mutation operation GraphQL::Client::Definition.
  #              Client.parse("query { version }") returns a definition.
  # variables - Optional set of variables to use during the operation.
  #             (default: {})
  #
  # Returns a structured query result or raises if the request failed.
  def query(definition, variables = {})
    response = RUGClient::Client.query(definition, variables: variables, context: {})

    if response.errors.any?
      raise QueryError, response.errors[:data].join(', ')
    else
      response.data
    end
  end
end
