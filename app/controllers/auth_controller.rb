class AuthController < ApplicationController
  def login
    redirect_to root_path if logged_in?
  end

  LoginUserMutation = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
      mutation($username: String!, $password: String!) {
        loginUser(input: {
          credentials: {
            username: $username,
            password: $password
          }
        })
        {
          token
          user {
            firstName
            lastName
            username
          }
          message
        }
      }
      
  GRAPHQL

  def login_user
    @username = params[:auth][:username].downcase
    @password = params[:auth][:password].downcase

    data = query LoginUserMutation, username: @username, password: @password

    if !data.login_user
      flash[:message] = 'The username or password is incorrect'
      flash[:type] = 'danger'
      redirect_to login_path
    else
      flash[:message] = data.login_user.message
      flash[:type] = 'success'
      redirect_to root_path
    end
  end

  LogoutUserMutation = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
      mutation {
        logoutUser(input: {})
        {
          message
        }
      }
  GRAPHQL

  def logout_user
    redirect_to root_path unless logged_in?
    data = query LogoutUserMutation

    if !data.logout_user
      flash[:message] = 'There\'s some error occured. It\'s not your fault'
      flash[:type] = 'danger'
      redirect_to root_path
    else
      flash[:message] = data.logout_user.message
      flash[:type] = 'success'
      redirect_to root_path
    end
  end

  RegisterUserMutation = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
      mutation($firstName: String!, $lastName: String!, $username: String!, $password: String!) {
        registerUser(input: {
          firstName: $firstName,
          lastName: $lastName,
          credentials: {
            username: $username,
            password: $password
          }
        })
        {
          user {
            firstName
            lastName
            username
          }
          message
        }
      }
  GRAPHQL

  def register
  end

  def register_user
    @first_name = params[:auth][:first_name].downcase
    @last_name = params[:auth][:last_name].downcase
    @username = params[:auth][:username].downcase
    @password = params[:auth][:password].downcase

    data = query RegisterUserMutation, firstName: @first_name, lastName: @last_name, username: @username,
                                       password: @password

    if !data.register_user
      flash[:message] = data.errors.messages[:registerUser].join(', ')
      flash[:type] = 'danger'
      redirect_to register_path
    else
      flash[:message] = data.register_user.message
      flash[:type] = 'success'
      redirect_to users_path
    end
  end
end
