class HomeController < ApplicationController
  # All queries MUST be assigned to constants and therefore be statically
  # defined. Queries MUST NOT be generated at request time.
  RandomUserQuery = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
    query {
        randomUser {
            id
            username
            firstName
            lastName
        }
    }
  GRAPHQL

  AllUsers = RUGClient::Client.parse <<-'GRAPHQL'
    # All read requests are defined in a "query" operation
    query {
        users {
            id
            username
            firstName
            lastName
        }
    }
  GRAPHQL

  def index
    @random_user = (query RandomUserQuery).random_user
  end

  def all_users
    @users = (query AllUsers).users
  end
end
