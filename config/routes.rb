Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#index'
  get '/users', to: 'home#all_users'

  get '/admin', to: 'admin#index'

  get '/login', to: 'auth#login'
  get '/logout', to: 'auth#logout_user'
  post 'login/', to: 'auth#login_user'
  get '/register', to: 'auth#register'
  post '/register', to: 'auth#register_user'
end
